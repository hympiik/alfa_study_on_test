/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numsinfile;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Сергей Козелков
 */
public class NumsInFile {
    static String fileName = "nums.txt";    //имя файла для чисел
    static int numsCount = 20;              //кол-во чисел 
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 0 || (args.length > 0 && args[0].equals("help"))){
           System.out.println("---------------------------  Help  ----------------------------");
           System.out.println("Program need start with parameters");
           System.out.println("Parameter 1 - operation, is:");
           System.out.println("  1- Create file and fill with numbers");
           System.out.println("  2- Read file, order values from low to hight, output to console");
           System.out.println("  3- Read file, order values from hight to low, output to console");
           
           System.out.println();
           System.out.println("Parameter 2- file name with numbers");
           System.out.println("---------------------------------------------------------------");
        }
        
        //второй параметр: имя файла
        if (args.length > 1)
           fileName = args[1];
        
        //1 - запись в файл случайных значений
        if (args.length > 0 && args[0].equals("1"))
            writeToFile(fileName);  
        
        //2 - Чтение из файла, сортировка по-возрастанию, вывод
        if (args.length > 0 && args[0].equals("2")){
            //чтение из файла
           int[] nums = readFromFile(fileName);  
           
           //сортировка
           orderNums(nums, true);
           
           //вывод в консоль
           numsToConsole(nums);
        }
        
        //3 - Чтение из файла, сортировка по-убыванию, вывод
        if (args.length > 0 && args[0].equals("3")){
            //чтение из файла
           int[] nums = readFromFile(fileName); 
           
           //сортировка
           orderNums(nums, false);
           
           //вывод в консоль
           numsToConsole(nums);
        }
    }
    
    //запись чисел в файл в случайном порядке
    private static void writeToFile(String fileName){
      try(FileWriter writer = new FileWriter(fileName, false))
        {
            //формирование последовательности чисел 
            String[] nums = new String[numsCount];
            for (int i = 0; i < nums.length; i++)    
                nums[i] = String.valueOf(i + 1);       //все числа хранятся подряд                   
                
            //извлечение случайного значения
            int k = nums.length;
            while(k > 0){                              //пытаемся снова извлечь случайную строку 
                                                       //  пока не будут извлечены все строки из nums
               Random rand = new Random();
               int i = rand.nextInt(nums.length);      //случайная позиция в массиве
               if (nums[i].length() > 0)               //есть ли чтото в этой позиции
               {
                  writer.write(nums[i] + (k>1?",":""));//запись в файл этой позиции
                  nums[i] = "";                        //удаление, чтобы в следующий раз не извлекать 
                  k--;
               }
            }
            writer.flush();
        }
        catch(IOException ex){
             
            System.out.println(ex.getMessage());
        } 
    }
    
    //чтение чисел из файла
    private static int[] readFromFile(String fileName){
      int[] ret = new int[numsCount];  
        
      try(FileReader reader = new FileReader(fileName))
        {
           // читаем посимвольно
            int c;
            String s = "";
            int i = 0;
            while((c=reader.read())!=-1){
                if ((char)c == ','){            //встретилась запятая
                   if (i < ret.length)               //чтобы не передали нам больше чем нужно 
                     ret[i++] = Integer.parseInt(s); //добавить число в массив 
                   s = "";
                }else{
                   s = s + (char)c;           
                }
            } 
            ret[i++] = Integer.parseInt(s);         //последнее число
        }
        catch(IOException ex){
             
            System.out.println(ex.getMessage());
        }   
      
      return ret;
    }
    
    //вывод чисел в консоль
    static void numsToConsole(int[] nums){
      for (int i = 0; i < nums.length; i++)
         System.out.print(nums[i] + (i < nums.length - 1?",":"")); 
      System.out.println();
    }
    
    //сортировка чисел
    static void orderNums(int[] nums, boolean isAsc){
      boolean isChange;
      do{
        isChange = false;   
        for (int i = 0; i < nums.length - 1; i++){
          if (( isAsc && nums[i] > nums[i + 1]) ||          //по-возрастанию
              (!isAsc && nums[i] < nums[i + 1]))            //по-убыванию
          {
               int tmp = nums[i];
               nums[i] = nums[i + 1];
               nums[i + 1] = tmp;
               isChange = true; 
          }
        }
      }while(isChange);
    }
}
